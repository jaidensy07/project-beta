import React, { Component } from "react";
import { Link, NavLink } from 'react-router-dom';

class SearchCar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: [],
            search_key: "",
            searchColumns: [[], [], []],
        };

        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event){
        const newState = {};
        newState[event.target.id] = event.target.value;
        this.setState(newState);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            this.setState({ models: data.model});
        }
    }

    render(){
        return (
        <React.Fragment>
        <div className="shadow p-4 mt-4">
          <h1>Search Cars</h1>
            <form id="model-search-form">
                <div className="input-group">
                    <input
                        value={this.state.search_key}
                        onChange={this.handleChange}
                        type="search"
                        className="form-control rounded"
                        placeholder="Enter vehicle model"
                        id="search_key"
                    />
                </div>
            </form>
            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
                <ul className="navbar-nav navbar-brand">
                    <li className="nav-item dropdown">
                        <NavLink
                          className="nav-link dropdown-toggle"
                          to="#"
                          id="navbarPriceLink"
                          role="button"
                          data-bs-toggle="dropdown"
                          aria-expanded="false"
                          >
                            Price
                          </NavLink>
                          <ul
                            className="dropdown-menu dropdown-menu-dark"
                            aria-labelledby="navbarPriceLink"
                           >
                            
                           </ul>
                    </li>
                </ul>
            </div>
        </div>
      </React.Fragment>
        )
    }
}

export default SearchCar;