import React from "react";
import { Link } from "react-router-dom";

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      models: [],
      sales: [],
    };
  }
  
  async getListCars() {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        this.setState({
          models: data.models,
        });
      } else {
        console.error(response);
      }
  } 

  async getListSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        this.setState({
          sales: data.sales,
        }); 
      } else {
        console.error(response);
      }
  }

  async componentDidMount() {
    this.getListCars();
    this.getListSales();
  }


  render () {
    console.log(this.state.sales)
    let list_of_models = []
    this.state.sales.map(sale => list_of_models.push(sale.automobile.model.name))
    console.log(list_of_models)
    
    let mf = 1;
    let m = 0;
    let item;
    for (let i=0; i<list_of_models.length; i++)
    {
      for (let j=i; j<list_of_models.length; j++)
      {
        if (list_of_models[i] == list_of_models[j])
        m++;
        if (mf<m)
        {
          mf=m; 
          item = list_of_models[i];
        }
      }
      m=0;
    }
    let best_seller = item
    console.log("this is the best seller", best_seller)
  return (
    <React.Fragment>
    <div className="container my-5 hero white shadow" id="splashpage">
      <div className="hero container-img">

        <img src="cars.jpg" className="container" alt="loading"/>
          <div className="strokeme">
            <h1 className="display-5 fw-bold centered-high">CarCar</h1>
            <p className="lead mb-4 centered">
              The premiere solution for automobile dealership
              management!
            </p>
          </div>

      </div>

      <div className="two-column my-5 container my-5 hero white shadow">
        <div className="pe-3">
        {this.state.models.map(model => {
              if (model.name === best_seller) {
                return (
                  <img key={model.id} src={ model.picture_url }/>
                );
              }
              })}
        </div>
        
        <div className="ps-3 black-text center-grid ">
          <h1>TOP SELLER!</h1>
          {this.state.models.map(model => {
              if (model.name === best_seller) {
                return (
                  <div>The {model.manufacturer.name} {model.name} is our current top seller,
                  check it out in our vehicles page!</div>
                );
              }
              })}
        </div>
      </div>
    </div>
    </React.Fragment>
    );
  }
}

export default MainPage;
